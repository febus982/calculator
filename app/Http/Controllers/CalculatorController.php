<?php

namespace App\Http\Controllers;

use App\Calculator\Calculator;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CalculatorController extends Controller
{
    public function render(Request $request): View
    {
        return view('calculator');
    }

    public function calculate(Request $request)
    {
        $calculator = new Calculator(
            $request->input('firstValue'),
            $request->input('secondValue'),
            $request->input('operator')
        );

        $validator = $calculator->validate();

        if ($validator->fails()) {
            return redirect('')
                ->withErrors($validator)
                ->withInput();
        }

        return back()
            ->with('result', $calculator->calculate())
            ->withInput();
    }
}
