<?php

namespace App\Calculator\Operations;

use Illuminate\Support\Facades\Validator as ValidatorFactory;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;

class BitwiseOr implements OperationInterface
{
    const READABLE_OPERATOR = "bitwise_or";

    private $firstValue;
    private $secondValue;

    public function __construct($firstValue, $secondValue)
    {
        $this->firstValue = $firstValue;
        $this->secondValue = $secondValue;
    }

    public function result()
    {
        $validation = $this->validate();
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        return decbin(bindec($this->firstValue) | bindec($this->secondValue));
    }

    public function validate(): Validator
    {
        /** @var Validator $validator */
        $validator = ValidatorFactory::make(
            [
                'firstValue' => $this->firstValue,
                'secondValue' => $this->secondValue,
            ],
            [
                'firstValue' => 'required|string|regex:#^[01]+$#',
                'secondValue' => 'required|string|regex:#^[01]+$#',
            ]
        );

        return $validator;
    }
}
