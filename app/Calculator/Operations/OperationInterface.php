<?php

namespace App\Calculator\Operations;

use Illuminate\Validation\Validator;

interface OperationInterface
{
    public function __construct($firstValue, $secondValue);

    public function validate(): Validator;

    public function result();
}
