<?php

namespace App\Calculator\Operations;

use Illuminate\Support\Facades\Validator as ValidatorFactory;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;

class Sum implements OperationInterface
{
    const READABLE_OPERATOR = "sum";

    private $firstValue;
    private $secondValue;

    public function __construct($firstValue, $secondValue)
    {
        $this->firstValue = $firstValue;
        $this->secondValue = $secondValue;
    }

    public function result()
    {
        $validation = $this->validate();
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        return $this->firstValue + $this->secondValue;
    }

    public function validate(): Validator
    {
        /** @var Validator $validator */
        $validator = ValidatorFactory::make(
            [
                'firstValue' => $this->firstValue,
                'secondValue' => $this->secondValue,
            ],
            [
                'firstValue' => 'required|numeric',
                'secondValue' => 'required|numeric',
            ]
        );

        return $validator;
    }
}
