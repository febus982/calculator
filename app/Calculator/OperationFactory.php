<?php

namespace App\Calculator;

use App\Calculator\Operations\BitwiseAnd;
use App\Calculator\Operations\BitwiseOr;
use App\Calculator\Operations\Division;
use App\Calculator\Operations\Multiplication;
use App\Calculator\Operations\Subtraction;
use App\Calculator\Operations\Sum;
use App\Exceptions\CalculatorOperationNotImplementedException;

class OperationFactory
{
    const OPERATIONS = [
        Sum::READABLE_OPERATOR => Sum::class,
        Subtraction::READABLE_OPERATOR => Subtraction::class,
        Multiplication::READABLE_OPERATOR => Multiplication::class,
        Division::READABLE_OPERATOR => Division::class,
        BitwiseAnd::READABLE_OPERATOR => BitwiseAnd::class,
        BitwiseOr::READABLE_OPERATOR => BitwiseOr::class,
    ];

    public static function make($firstValue, $secondValue, $operator)
    {
        if (
            array_key_exists($operator, self::OPERATIONS)
            && app()->bound($operator)
        ) {
            $class = self::OPERATIONS[$operator];
            return new $class($firstValue, $secondValue);
        }

        throw new CalculatorOperationNotImplementedException();
    }

    public static function getAllowedOperations(): array
    {
        return array_keys(self::OPERATIONS);
    }
}
