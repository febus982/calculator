<?php

namespace App\Calculator;

use App\Calculator\Operations\OperationInterface;
use Illuminate\Support\Facades\Validator as ValidatorFactory;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;

class Calculator
{
    private $firstValue;
    private $secondValue;
    private $operator;

    public function __construct($firstValue, $secondValue, $operator)
    {
        $this->firstValue = $firstValue;
        $this->secondValue = $secondValue;
        $this->operator = $operator;
    }

    /**
     * @return integer|float
     * @throws ValidationException
     */
    public function calculate()
    {
        $validator = $this->validate();
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        /** @var OperationInterface $operation */
        $operation = app($this->operator, [$this->firstValue, $this->secondValue]);

        return $operation->result();
    }

    public function validate(): Validator
    {
        /** @var Validator $validator */
        $validator = ValidatorFactory::make(
            [
                'operator' => $this->operator,
                'firstValue' => $this->firstValue,
                'secondValue' => $this->secondValue,
            ],
            [
                'operator' => [
                    'required',
                    Rule::in(OperationFactory::getAllowedOperations()),
                ],
                'firstValue' => 'required',
                'secondValue' => 'required',
            ]
        );

        $this->validateOperationValues($validator);

        return $validator;
    }

    private function validateOperationValues(Validator $validator): void
    {
        $validator->after(function (Validator $validator) {
            /** @var OperationInterface $operation */
            $operation = app(
                $this->operator,
                [
                    $validator->attributes()['firstValue'],
                    $validator->attributes()['secondValue'],
                ]
            );

            $operationValidation = $operation->validate();

            if ($operationValidation->fails()) {
                $validator->getMessageBag()->merge($operationValidation);
            }
        });
    }

}
