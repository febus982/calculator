<?php

namespace App\Providers;

use App\Calculator\OperationFactory;
use Illuminate\Support\ServiceProvider;

class OperationsServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        foreach (OperationFactory::OPERATIONS as $operator => $class) {
            $this->app->bind($operator, function ($app, $parameters) use ($operator) {
                return OperationFactory::make($parameters[0], $parameters[1], $operator);
            });
        }
    }
}
