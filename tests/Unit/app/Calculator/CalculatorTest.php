<?php

namespace Tests\Unit\app\Calculator;

use App\Calculator\Calculator;
use App\Calculator\Operations\BitwiseAnd;
use App\Calculator\Operations\Multiplication;
use Tests\TestCase;

class CalculatorTest extends TestCase
{
    public function testWillPassArithmeticValidationWithCorrectValues()
    {
        $arithmeticCalculator = new Calculator(
            3.14,
            -2,
            Multiplication::READABLE_OPERATOR
        );

        $this->assertFalse(
            $arithmeticCalculator->validate()->fails()
        );
    }

    public function testWillPassBitwiseValidationWithCorrectValues()
    {
        $bitwiseCalculator = new Calculator(
            '10010',
            '10100',
            BitwiseAnd::READABLE_OPERATOR
        );

        $this->assertFalse(
            $bitwiseCalculator->validate()->fails()
        );
    }

    public function testWillFailArithmeticValidationWithWrongValues()
    {
        $arithmeticCalculator = new Calculator(
            3,
            true,
            Multiplication::READABLE_OPERATOR
        );

        $this->assertTrue(
            $arithmeticCalculator->validate()->fails()
        );
    }

    public function testWillFailBitwiseValidationWithWrongValues()
    {
        $bitwiseCalculator = new Calculator(
            '30300',
            10010,
            BitwiseAnd::READABLE_OPERATOR
        );

        $this->assertTrue(
            $bitwiseCalculator->validate()->fails()
        );
    }


}
