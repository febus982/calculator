<?php

namespace Tests\Unit\app\Calculator;

use App\Calculator\OperationFactory;
use App\Calculator\Operations\Sum;
use App\Exceptions\CalculatorOperationNotImplementedException;
use Tests\TestCase;

class OperationFactoryTest extends TestCase
{
    public function testWillThrowExceptionIfOperationIsNotImplemented()
    {
        $this->expectException(CalculatorOperationNotImplementedException::class);

        OperationFactory::make(
            3.14,
            -2,
            'NotImplementedOperation'
        );
    }

    public function testWillCreateOperationCorrectlyWhenImplemented()
    {
        $operation = OperationFactory::make(
            3.14,
            -2,
            Sum::READABLE_OPERATOR
        );

        $this->assertSame(Sum::class, get_class($operation));
    }
}
