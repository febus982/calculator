<?php

namespace Tests\Unit\app\Calculator\Operations;

use App\Calculator\Operations\Multiplication;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class MultiplicationTest extends TestCase
{
    public function testWillCalculateCorrectlyPositiveNumbers()
    {
        $operation = new Multiplication(
            3,
            2
        );

        $this->assertEquals(6, $operation->result());
        $this->assertFalse($operation->validate()->fails());
    }

    public function testWillCalculateCorrectlyNegativeNumbers()
    {
        $operation = new Multiplication(
            3,
            -2
        );

        $this->assertEquals(-6, $operation->result());
        $this->assertFalse($operation->validate()->fails());
    }

    public function testWillCalculateCorrectlyFloatNumbers()
    {
        $operation = new Multiplication(
            3,
            -1.5
        );

        $this->assertEquals(-4.5, $operation->result());
        $this->assertFalse($operation->validate()->fails());
    }

    public function testWillCalculateCorrectlyZeroNumbers()
    {
        $operation = new Multiplication(
            0,
            -1.5
        );

        $this->assertEquals(0, $operation->result());
        $this->assertFalse($operation->validate()->fails());
    }

    public function testWillFailWhenStringsAreSubmitted()
    {
        $operation = new Multiplication(
            'test',
            -1.5
        );

        $this->assertTrue($operation->validate()->fails());
        $this->expectException(ValidationException::class);
        $operation->result();
    }
}
