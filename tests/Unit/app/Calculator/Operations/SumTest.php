<?php

namespace Tests\Unit\app\Calculator\Operations;

use App\Calculator\Operations\Sum;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class SumTest extends TestCase
{
    public function testWillCalculateCorrectlyPositiveNumbers()
    {
        $operation = new Sum(
            3,
            2
        );

        $this->assertEquals(5, $operation->result());
        $this->assertFalse($operation->validate()->fails());
    }

    public function testWillCalculateCorrectlyNegativeNumbers()
    {
        $operation = new Sum(
            3,
            -2
        );

        $this->assertEquals(1, $operation->result());
        $this->assertFalse($operation->validate()->fails());
    }

    public function testWillCalculateCorrectlyFloatNumbers()
    {
        $operation = new Sum(
            3,
            -1.5
        );

        $this->assertEquals(1.5, $operation->result());
        $this->assertFalse($operation->validate()->fails());
    }

    public function testWillCalculateCorrectlyZeroNumbers()
    {
        $operation = new Sum(
            0,
            -1.5
        );

        $this->assertEquals(-1.5, $operation->result());
        $this->assertFalse($operation->validate()->fails());
    }

    public function testWillFailWhenStringsAreSubmitted()
    {
        $operation = new Sum(
            'test',
            -1.5
        );

        $this->assertTrue($operation->validate()->fails());
        $this->expectException(ValidationException::class);
        $operation->result();
    }
}
