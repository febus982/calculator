<?php

namespace Tests\Unit\app\Calculator\Operations;

use App\Calculator\Operations\BitwiseOr;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class BitwiseOrTest extends TestCase
{
    public function testWillCalculateCorrectlyPositiveNumbers()
    {
        $operation = new BitwiseOr(
            '10101',
            '1000'
        );

        $this->assertEquals('11101', $operation->result());
        $this->assertFalse($operation->validate()->fails());
    }

    public function testWillFailIfNegativeNumbersAreProvided()
    {
        $operation = new BitwiseOr(
            '10101',
            '-100'
        );

        $this->assertTrue($operation->validate()->fails());
        $this->expectException(ValidationException::class);
        $operation->result();
    }

    public function testWillFailIfFloatBinaryStringAreProvided()
    {
        $operation = new BitwiseOr(
            '10010.1',
            '10000'
        );

        $this->assertTrue($operation->validate()->fails());
        $this->expectException(ValidationException::class);
        $operation->result();
    }

    public function testWillCalculateCorrectlyZeroNumbers()
    {
        $operation = new BitwiseOr(
            '0',
            '10100'
        );

        $this->assertEquals('10100', $operation->result());
        $this->assertFalse($operation->validate()->fails());
    }

    public function testWillFailWhenNotStringsAreSubmitted()
    {
        $operation = new BitwiseOr(
            '10110',
            10
        );

        $this->assertTrue($operation->validate()->fails());
        $this->expectException(ValidationException::class);
        $operation->result();
    }
}
