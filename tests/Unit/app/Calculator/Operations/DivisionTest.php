<?php

namespace Tests\Unit\app\Calculator\Operations;

use App\Calculator\Operations\Division;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class DivisionTest extends TestCase
{
    public function testWillCalculateCorrectlyPositiveNumbers()
    {
        $operation = new Division(
            3,
            2
        );

        $this->assertEquals(1.5, $operation->result());
        $this->assertFalse($operation->validate()->fails());
    }

    public function testWillCalculateCorrectlyNegativeNumbers()
    {
        $operation = new Division(
            3,
            -2
        );

        $this->assertEquals(-1.5, $operation->result());
        $this->assertFalse($operation->validate()->fails());
    }

    public function testWillCalculateCorrectlyFloatNumbers()
    {
        $operation = new Division(
            3,
            1.5
        );

        $this->assertEquals(2, $operation->result());
        $this->assertFalse($operation->validate()->fails());
    }

    public function testWillCalculateCorrectlyZeroNumbers()
    {
        $operation = new Division(
            0,
            -1.5
        );

        $this->assertEquals(0, $operation->result());
        $this->assertFalse($operation->validate()->fails());

        $operation2 = new Division(
            3,
            0
        );

        $this->assertTrue($operation2->validate()->fails());
        $this->expectException(ValidationException::class);
        $operation2->result();
    }

    public function testWillFailWhenStringsAreSubmitted()
    {
        $operation = new Division(
            'test',
            -1.5
        );

        $this->assertTrue($operation->validate()->fails());
        $this->expectException(ValidationException::class);
        $operation->result();
    }
}
