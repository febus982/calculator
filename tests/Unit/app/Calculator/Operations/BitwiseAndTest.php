<?php

namespace Tests\Unit\app\Calculator\Operations;

use App\Calculator\Operations\BitwiseAnd;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class BitwiseAndTest extends TestCase
{
    public function testWillCalculateCorrectlyPositiveNumbers()
    {
        $operation = new BitwiseAnd(
            '10101',
            '100'
        );

        $this->assertEquals('100', $operation->result());
        $this->assertFalse($operation->validate()->fails());
    }

    public function testWillFailIfNegativeNumbersAreProvided()
    {
        $operation = new BitwiseAnd(
            '10101',
            '-100'
        );

        $this->assertTrue($operation->validate()->fails());
        $this->expectException(ValidationException::class);
        $operation->result();
    }

    public function testWillFailIfFloatBinaryStringAreProvided()
    {
        $operation = new BitwiseAnd(
            '10010.1',
            '10000'
        );

        $this->assertTrue($operation->validate()->fails());
        $this->expectException(ValidationException::class);
        $operation->result();
    }

    public function testWillCalculateCorrectlyZeroNumbers()
    {
        $operation = new BitwiseAnd(
            '0',
            '10100'
        );

        $this->assertEquals('0', $operation->result());
        $this->assertFalse($operation->validate()->fails());
    }

    public function testWillFailWhenNotStringsAreSubmitted()
    {
        $operation = new BitwiseAnd(
            '10110',
            10
        );

        $this->assertTrue($operation->validate()->fails());
        $this->expectException(ValidationException::class);
        $operation->result();
    }
}
