<?php

namespace Tests\Unit\app\Calculator\Operations;

use App\Calculator\Operations\Division;
use App\Calculator\Operations\Subtraction;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class SubtractionTest extends TestCase
{
    public function testWillCalculateCorrectlyPositiveNumbers()
    {
        $operation = new Subtraction(
            3,
            2
        );

        $this->assertEquals(1, $operation->result());
        $this->assertFalse($operation->validate()->fails());
    }

    public function testWillCalculateCorrectlyNegativeNumbers()
    {
        $operation = new Subtraction(
            3,
            -2
        );

        $this->assertEquals(5, $operation->result());
        $this->assertFalse($operation->validate()->fails());
    }

    public function testWillCalculateCorrectlyFloatNumbers()
    {
        $operation = new Subtraction(
            3,
            1.5
        );

        $this->assertEquals(1.5, $operation->result());
        $this->assertFalse($operation->validate()->fails());
    }

    public function testWillCalculateCorrectlyZeroNumbers()
    {
        $operation = new Subtraction(
            0,
            -1.5
        );

        $this->assertEquals(1.5, $operation->result());
        $this->assertFalse($operation->validate()->fails());

        $operation2 = new Division(
            3,
            0
        );

        $this->assertTrue($operation2->validate()->fails());
        $this->expectException(ValidationException::class);
        $operation2->result();
    }

    public function testWillFailWhenStringsAreSubmitted()
    {
        $operation = new Subtraction(
            'test',
            -1.5
        );

        $this->assertTrue($operation->validate()->fails());
        $this->expectException(ValidationException::class);
        $operation->result();
    }
}
