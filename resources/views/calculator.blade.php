<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>

        <title>Calculator</title>
    </head>
    <body>
        <div class="position-ref full-height">
            <div class="content">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="title m-b-md">
                    Calculator
                </div>
                <form method="POST" action="/calculate">
                    @csrf
                    <label for="firstValue">First Value</label>
                    <input type="textfield" name="firstValue" value="{{ old('firstValue') }}" required />
                    <label for="operator">Operator</label>
                    <select type="options" name="operator" required >
                        @foreach (\App\Calculator\OperationFactory::getAllowedOperations() as $operator)
                            <option value="{{ $operator }}" {{ old('operator') == $operator ? 'selected' : '' }}>{{ $operator }}</option>
                        @endforeach
                    </select>
                    <label for="secondValue">Second Value</label>
                    <input type="textfield" name="secondValue" value="{{ old('firstValue') }}" required />
                    <input type="submit" />
                </form>

                @if (!is_null(session('result')))
                    <div class="alert">
                        <p>Result is: {{ session('result') }}</p>
                    </div>
                @endif
            </div>
        </div>
    </body>
</html>
