## Calculator
A simple calculator built with  [Laravel](http://laravel.com) framework

### Requirements
   - PHP >= 7.1.3
   - [Composer](https://getcomposer.org/)

## Installation Steps

#### Clone the repo
```
$ git clone https://bitbucket.org/febus982/calculator.git
$ cd calculator
```

#### Install Application dependencies
```
$ /path/to/composer install
```


#### Run the Application
```
/path/to/php artisan serve
```

The application will run on http://localhost:8000

#### Run Tests
Application is covered by unit tests. To run the tests, run the following command in the terminal.
   
```
vendor/bin/phpunit
```
